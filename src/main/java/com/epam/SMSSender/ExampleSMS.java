package com.epam.SMSSender;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "AC9c91b9484b8dafe05e2ce80b0dc1a13b";
    public static final String AUTH_TOKEN = "08269c471f3ba5183c6607dbbd272e0e";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380995136355"),
                new PhoneNumber("+13342768916"), str) .create();
    }
}
